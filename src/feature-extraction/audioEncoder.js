import * as tf from '@tensorflow/tfjs';
import * as speechCommands from 'lml-speech-commands';

let baseRecognizer;
let transferRecognizer;

(async function initSpeechCommands() {

    baseRecognizer = speechCommands.create('BROWSER_FFT');
    await baseRecognizer.ensureModelLoaded();

    transferRecognizer = baseRecognizer.createTransfer("audio-model");

    console.log("transferRecognizer");
    console.log(transferRecognizer);
})()

export function playRawAudio(rawAudio){
    return speechCommands.utils.playRawAudio(rawAudio);
}

export function collectExample(label) {

    let options = {
        includeRawAudio: true,
    };

    return transferRecognizer.collectExample(label, options).then(spectrogram => {
        console.log("COLLECT EXAMPLE");
        console.log(transferRecognizer.dataset.examples);

        const example = Object.values(transferRecognizer.dataset.examples)[0];
        // elimino el elemento del dataset para que no se vayan acumulando y, así,
        // ahorrar memoria. Esto lo hago porque solo uso el `transferReconizer` para
        // recolectar muestras de audio, no para hacer transfer learning, que es el
        // objetivo principal de este objeto.    
        const key = Object.keys(transferRecognizer.dataset.examples)[0];
        delete transferRecognizer.dataset.examples[key];

        return example;
    });
}
export function audioEncoder(audioDataSet) {

    let embeddingsPromise = [];
    for (let spec of audioDataSet) {
        const x = tf.tensor4d(
            spec.spectrogram.data, [1].concat(baseRecognizer.modelInputShape().slice(1)));

        let embedding = baseRecognizer.recognize(x, { includeEmbedding: true })
            .then(result => {
                return result.embedding.reshape([result.embedding.shape[1]]);
            });

        embeddingsPromise.push(embedding);
    }

    return Promise.all(embeddingsPromise).then(features => {
        return tf.stack(features);
    })
}